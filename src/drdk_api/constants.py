DR_ROOT_URL = "https://dr.dk"
API_ROOT_URL = f"{DR_ROOT_URL}/mu-online/api/1.4"

API_SEARCH_LIMIT = 70
API_LIST_LIMIT = 70
